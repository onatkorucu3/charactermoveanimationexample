﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationStateController : MonoBehaviour
{
    private Animator _animator;

    private int _isWalkingHash;
    private int _isRunningHash;
    private int _velocityHash;

    private float _velocity;
    [SerializeField] private float _acceleration = 0.1f;
    [SerializeField] private float _deceleration = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _isWalkingHash = Animator.StringToHash("isWalking");
        _isRunningHash = Animator.StringToHash("isRunning");
        _velocityHash = Animator.StringToHash("Velocity");
    }

    // Update is called once per frame
    void Update()
    {
        bool isForwardPressed = Input.GetKey("w");
        /*bool isRunPressed = Input.GetKey("left shift");

        bool isWalking = _animator.GetBool(_isWalkingHash);
        bool isRunning = _animator.GetBool(_isRunningHash);

        if (!isRunning && !isWalking && isForwardPressed)
        {
            _animator.SetBool(_isWalkingHash, true);
        }
        
        if (!isRunning && isWalking && !isForwardPressed)
        {
            _animator.SetBool(_isWalkingHash, false);
        }
        
        if (!isRunning && isForwardPressed && isRunPressed)
        {
            _animator.SetBool(_isRunningHash, true);
        }
        
        if (isRunning && (!isForwardPressed || !isRunPressed))
        {
            _animator.SetBool(_isRunningHash, false);
        }*/
        
        if (isForwardPressed && _velocity <1)
        {
            _velocity += Time.deltaTime * _acceleration;
        }
        
        if (!isForwardPressed && _velocity > Mathf.Epsilon)
        {
            _velocity -= Time.deltaTime * _acceleration;
        }
        
        if (_velocity < 0)
        {
            _velocity = 0;
        }
        
        _animator.SetFloat(_velocityHash, _velocity);

    }
}
