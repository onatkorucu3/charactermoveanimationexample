﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoDimensionalAnimationStateController : MonoBehaviour
{
    [SerializeField] private float _velocityX = 0.0f;
    [SerializeField] private float _velocityZ = 0.0f;
    [SerializeField] private float _acceleration = 2.0f;
    [SerializeField] private float _deceleration = 2.0f;
    [SerializeField] private float _maximumWalkVelocity = 0.5f;
    [SerializeField] private float _maximumRunVelocity = 2.0f;

    private int velocityXHash;
    private int velocityZHash;
    
    private Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        
        velocityXHash = Animator.StringToHash("Velocity X");
        velocityZHash = Animator.StringToHash("Velocity Z");

    }

    // Update is called once per frame
    void Update()
    {
        bool isForwardPressed = Input.GetKey(KeyCode.W);
        bool isLeftPressed = Input.GetKey(KeyCode.A);
        bool isRightPressed = Input.GetKey(KeyCode.D);
        bool isRunPressed = Input.GetKey(KeyCode.LeftShift);

        float currentMaxVelocity = isRunPressed ? _maximumRunVelocity : _maximumWalkVelocity;
        
        ChangeVelocity(isForwardPressed, isLeftPressed, isRightPressed, currentMaxVelocity);
        LockOrResetVelocity(isForwardPressed, isLeftPressed, isRightPressed, isRunPressed, currentMaxVelocity);

        _animator.SetFloat(velocityXHash, _velocityX);
        _animator.SetFloat(velocityZHash, _velocityZ);
    }

    void ChangeVelocity(bool isForwardPressed, bool isLeftPressed, bool isRightPressed, float currentMaxVelocity)
    {
        if (isForwardPressed && _velocityZ < currentMaxVelocity)
        {
            _velocityZ += _acceleration * Time.deltaTime;
        }

        if (isLeftPressed && _velocityX > -currentMaxVelocity)
        {
            _velocityX -= _acceleration * Time.deltaTime;
        }        
        
        if (isRightPressed && _velocityX < currentMaxVelocity)
        {
            _velocityX += _acceleration * Time.deltaTime;
        }

        if (!isForwardPressed && _velocityZ > 0.0f)
        {
            _velocityZ -= _deceleration * Time.deltaTime;
        }

        if (!isLeftPressed && _velocityX < 0.0f)
        {
            _velocityX += _deceleration * Time.deltaTime;
        }

        if (!isRightPressed && _velocityX > 0.0f)
        {
            _velocityX -= _deceleration * Time.deltaTime;
        }
    }

    void LockOrResetVelocity(bool isForwardPressed, bool isLeftPressed, bool isRightPressed, bool isRunPressed, float currentMaxVelocity)
    {
        if (!isForwardPressed && _velocityZ < 0.0f)
        {
            _velocityZ = 0.0f;
        }

        if (!isLeftPressed && !isRightPressed && _velocityX != 0.0f && _velocityX < 0.05f && _velocityX > -0.05f)
        {
            _velocityX = 0.0f;
        }

        if (isForwardPressed && isRunPressed && _velocityZ > currentMaxVelocity)
        {
            _velocityZ = currentMaxVelocity;
        }else if(isForwardPressed && _velocityZ > currentMaxVelocity)
        {
            _velocityZ -= _deceleration * Time.deltaTime;
            if (_velocityZ > currentMaxVelocity && _velocityZ < currentMaxVelocity + 0.05)
            {
                _velocityZ = currentMaxVelocity;
            }
        }else if (isForwardPressed && _velocityZ < currentMaxVelocity && _velocityZ > currentMaxVelocity - 0.05)
        {
            _velocityZ = currentMaxVelocity;
        }
        
        if (isLeftPressed && isRunPressed && _velocityX < -currentMaxVelocity)
        {
            _velocityX = -currentMaxVelocity;
        }else if(isLeftPressed && _velocityX < -currentMaxVelocity)
        {
            _velocityX += _deceleration * Time.deltaTime;
            if (_velocityX < -currentMaxVelocity && _velocityX > -(currentMaxVelocity + 0.05))
            {
                _velocityX = -currentMaxVelocity;
            }
        }else if (isLeftPressed && _velocityX > -currentMaxVelocity && _velocityX < -(currentMaxVelocity + 0.05))
        {
            _velocityX = -currentMaxVelocity;
        }
        
        
        if (isRightPressed && isRunPressed && _velocityX > currentMaxVelocity)
        {
            _velocityX = currentMaxVelocity;
        }else if(isRightPressed && _velocityX > currentMaxVelocity)
        {
            _velocityX -= _deceleration * Time.deltaTime;
            if (_velocityX > currentMaxVelocity && _velocityX < currentMaxVelocity + 0.05)
            {
                _velocityX = currentMaxVelocity;
            }
        }else if (isRightPressed && _velocityX < currentMaxVelocity && _velocityX > currentMaxVelocity - 0.05)
        {
            _velocityX = currentMaxVelocity;
        }
    }
}
